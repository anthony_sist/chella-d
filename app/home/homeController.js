/**
 * Created by asist on 6/7/2015.
 */
(function() {
    angular
        .module('newApp')
        .controller('homeController', homeController)

        homeController.$inject = ['mapService', 'facebookService', '$scope'];

        function homeController (mapService, facebookService, $scope) {

            //Assign viewmodel
            var vm = this;

            vm.feed = []; //facebook feed

            //Call map Service
            mapService.drawGoogleMap();

            //Call facebook feed
            activateFeed();

            function activateFeed() {
                return fbGet().then(function () {
                    console.log('facebook feed activated')
                })
            }

            function fbGet() {
                return facebookService.getFeed()
                    .then(function(data) {
                        vm.feed = data.data;
                        console.log(vm.feed)
                        return vm.feed;
                    })
            }

            //Find correct image size from feed
            function resizeImage(data) {
                data.forEach(function(item){

                })
            }
        }
})();