(function () {
    angular.module('newApp')
        .run(configure);

    //configure the states during the run phase as per https://github.com/johnpapa/angular-styleguide#routing

    /* @ngInject */
    function configure(routerHelper) {
        routerHelper.configureStates(getStates(), "/");
    }

    //These can (should?) be moved under each sections folder. i.e. each route within each directory.
    function getStates() {
        return [
            {
                state: 'home',
                config: {
                    templateUrl: '../views/home/partial-home.html',
                    url: '/',
                    controller: 'homeController',
                    controllerAs : 'vm'
                }
            },
            {
                state: 'about',
                config: {
                    templateUrl: '../views/about/partial-about.html',
                    url: '/about'
                }
            },
            {
                state: 'shop',
                config: {
                    templateUrl: '../views/shop/partial-shop.html',
                    url: '/shop'
                }
            },
            {
                state: 'consign',
                config: {
                    templateUrl: '../views/consign/partial-consign.html',
                    url: '/consign'
                }
            },
            {
                state: 'contact',
                config: {
                    templateUrl: '../views/contact/partial-contact.html',
                    url: '/contact',
                    controller: 'contactController',
                    controllerAs : 'vm'
                }
            }

        ]
    }
})();