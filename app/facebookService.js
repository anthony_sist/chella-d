(function() {
    angular
        .module('newApp')
        .factory('facebookService', facebookService);

    facebookService.$inject = ['$http'];

    function facebookService($http) {

        return {
            getFeed : getFeed
        };

        function getFeed () {
            return $http.get('https://graph.facebook.com/v2.3/chelladhhi/posts?access_token=1858016517757693|r9mbC41SRfOJ8VReyri1JqK0MqQ')
                        .then(getDataComplete)
                        .catch(getDataFailed)

            function getDataComplete (res) {
                return res.data;
            }

            function getDataFailed (error) {
                console.log(error);
            }

        }
    }
})();