/**
 * Created by asist on 6/14/2015.
 */
(function() {
    angular
        .module('newApp')
        .controller('contactController', contactController)

    contactController.$inject = ['mapService', '$scope'];

    function contactController (mapService, $scope) {

        //Assign viewmodel
        var vm = this;



        //Call map Service
        mapService.drawGoogleMap();


    }
})();