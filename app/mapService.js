(function() {angular
    .module('newApp')
    .service('mapService', mapService)

    mapService.$inject = ['$rootScope'];
function mapService($rootScope) {

    this.drawGoogleMap = function() {


        //-----Code for the map on the home page-----//

        var mapColorOptions = [{"featureType":"all","elementType":"all","stylers":[{"visibility":"simplified"},{"hue":"#ff0000"}]}];

        var styledMap = new google.maps.StyledMapType(mapColorOptions, {name: "Styled Map"})
        function initialize() {
            var mapOptions = {
                center: { lat: 32.173353, lng: -80.732903},
                zoom: 15,
                scrollwheel: false,
                mapTypeControlOptions : {
                    mapTypeIds : [google.maps.MapTypeId.ROADMAP, 'map_style']
                }
            };



            var map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);

            console.log(map)
            var myIcon = new google.maps.MarkerImage("img/logo.png", null, null, null, new google.maps.Size(80,45));

            var marker = new google.maps.Marker({
                position: { lat: 32.173353, lng: -80.732903},
                map: map,
                icon: myIcon
            });

            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');
        }
        initialize();
    };
}})();